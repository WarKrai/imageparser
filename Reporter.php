<?php

require_once("Checker.php");
require_once("Menu.php");

class Reporter
{
    private $url;

    public function __construct($url)
    {
        $this->url = Checker::parserUrlBuilder($url);
    }

    public function report()
    {
        if(!file_exists(Checker::generateFileName($this->url))) {
            print "Error!\n";
            print "Файл отчета не найден!\n";
            print "Нажмите Enter что бы продолжть...\n";
            readline();
            Menu::showMainMenu();
        }
        print "Состовляем отчет...";
        print "Ожидайте...";
        $links = array();
        $f = fopen(Checker::generateFileName($this->url), "r");
        while (($data = fgetcsv($f, 1000, ";")) != false) {
            array_push($links, $data);
        }
        fclose($f);
        Menu::clear_screen();
        if (count($links) == 0) {
            print "Для домена {$this->url} не найдено ссылок на изображения!\n";
            print "Нажмите Enter что бы продолжить...\n";
            readline();
            Menu::showMainMenu();
        } else {
            print "Все ссылки на изображения для {$this->url}: \n";
            foreach ($links as $link) {
                print $link[0] . ": " . $link[1] . "\n";
            }
            print "\nНажмите Enter что бы продолжить...\n";
            readline();
            Menu::showMainMenu();
        }
    }
}