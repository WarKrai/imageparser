<?php


class Checker
{
    public static function menuChecker(string $ans): bool
    {
        return preg_match("/^((parse)|(report))\s((https?:\/\/)?)[a-z0-9-]+(\.[a-z]+)+\/?$/", $ans);
    }

    public static function hrefChecker(string $href): bool
    {
        return preg_match("/^\/.+$/", $href);
    }

    public static function parserUrlBuilder(string $url): string
    {
        $result = "";
        if (preg_match("/^[a-z0-9-]+(\.[a-z]+)+\/?/", $url)) {
            $url = "https://" . $url;
        }
        $urlArray = str_split($url);
        if ($urlArray[count($urlArray) - 1] == "/") {
            for ($i = 0; $i < count($urlArray) - 1; $i++) {
                $result .= $urlArray[$i];
            }
            return $result;
        }
        return $url;
    }

    public static function imgSrcBuilder(string $src, string $url, string $domain): string
    {
        if (preg_match("/\/.+/", $src)) {
            return $url . "," . $domain . $src;
        } else {
            return $url . "," . $src;
        }
    }

    public static function generateFileName(string $url): string
    {
        $name = explode("//", $url);
        return "{$name[1]}.csv";
    }

    public static function checkConnection(string $url): bool
    {
        $html = file_get_contents($url);
        return !empty($html);
    }
}