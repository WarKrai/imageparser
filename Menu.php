<?php
require_once("Parser.php");
require_once("Checker.php");
require_once("Reporter.php");

class Menu
{
    public static function clear_screen()
    {
        if (stripos(php_uname("s"), "Windows")) {
            system("cls");
        } else {
            system("clear");
        }
    }

    public static function showMainMenu()
    {
        self::clear_screen();
        print "****************************************************************************\n";
        print "*                          Парсер изображений                              *\n";
        print "****************************************************************************\n";
        print "              Введите 'help' что бы получить справку о командах\n";
        $ans = self::menuAnswerParser(readline());
        switch ($ans["command"]) {
            case "parse":
                self::clear_screen();
                $parser = new Parser($ans["url"]);
                $parser->parse();
                break;
            case "report":
                self::clear_screen();
                $reporter = new Reporter($ans["url"]);
                $reporter->report();
                break;
            case "help": self::clear_screen(); self::showHelp(); break;
            case "exit": self::clear_screen(); exit(1); break;
            default:
                self::clear_screen();
                print "Input Error!\n";
                print "Нажмите Enter что бы продолжить...\n";
                readline();
                self::clear_screen();
                self::showMainMenu();
                break;
        }
    }

    private static function menuAnswerParser(string $ans): array
    {
        if ($ans == "help" || $ans == "exit"){
            return array("command" => $ans, "url" => "");
        } elseif (Checker::menuChecker($ans)) {
            return self::generateAnsArray($ans);
        } else {
            return array("command" => "error", "url" => "");
        }
    }

    private static function generateAnsArray($ans): array
    {
        $ans = explode(" ", $ans);
        return array("command" => $ans[0], "url" => $ans[1]);
    }

    private static function showHelp()
    {
        print "Справка по командам:\n";
        print "1. Команда parse - запускает парсер, принимает обязательный параметр url (как с протоколом, так и без).\n";
        print "2. Команда report - выводит в консоль результаты анализа для домена, принимает обязательный параметр domain (как с протоколом, так и без).\n";
        print "3. Команда help - выводит список команд с пояснениями.\n";
        print "4. Команда exit - завершает выполнение программы.\n";
        print "Нажмите Enter что бы продолжить...";
        readline();
        self::clear_screen();
        self::showMainMenu();
    }
}