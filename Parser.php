<?php


require_once("Checker.php");
require_once("Menu.php");

class Parser
{
    private $url;
    private $pagesArray;
    private $imagesArray;

    public function __construct(string $url)
    {
        $this->url = Checker::parserUrlBuilder($url);
        $this->pagesArray = array();
        $this->imagesArray = array();
    }

    private function getAllLinksOnPage(string $url): array
    {
        $clearHrefs = array();
        $hrefText = array();
        $html = file_get_contents($url);
        $hrefs = explode('<a',$html);
        unset($hrefs[0]);
        foreach($hrefs as $hrefsItem){
            $arHref = explode('href="',$hrefsItem);
            $arHref = explode('"',$arHref[1]);
            $hrefText[] = $arHref[0];
        }

        foreach($hrefText as $hrefTextItem){
            if($hrefTextItem!=''){
                $clearHrefs[]=$hrefTextItem;
            }
        }
        $clearHrefs = array_unique($clearHrefs);
        $resultArray = array();
        foreach($clearHrefs as $href) {
            if (Checker::hrefChecker($href)) {
                array_push($resultArray, $href);
            }
        }
        return $resultArray;
    }

    private function parseLinks(string $url)
    {
        array_push($this->pagesArray, $url);
        $linksOnThisPage = $this->getAllLinksOnPage($url);
        foreach ($linksOnThisPage as $link) {
            if (!in_array($this->url . $link, $this->pagesArray)) {
                $this->parseLinks($this->url . $link);
            }
        }
    }

    public function parse()
    {
        if (!Checker::checkConnection($this->url))
        {
            print "Error!\n";
            print "Не удалось получить данные с сайта!\n";
            print "Нажмите Enter что бы продолжить...\n";
            readline();
            Menu::showMainMenu();
        }
        print "Парсим страницы...\n";
        print "Ожидайте...\n";
        $this->parseLinks($this->url);
        print "Парсим изображения...\n";
        print "Ожидайте...\n";
        foreach ($this->pagesArray as $pages) {
            $this->parseImages($pages);
        }
        if (empty($this->imagesArray)) {
            print "Изображений на сайте не найдено!\n";
            print "Нажмите Enter что бы продолжить...\n";
            readline();
            Menu::showMainMenu();
        }
        $this->imagesArray = array_unique($this->imagesArray); 
        print "Записываем отчет в csv файл...\n";
        print "Ожидайте...\n";
        $f = fopen(Checker::generateFileName($this->url), "w");
        foreach($this->imagesArray as $images) {
            fputcsv($f, explode(',', $images), ";");
        }
        fclose($f);
        Menu::clear_screen();
        print "Отчет записан в файле: " . __DIR__ . "/" . Checker::generateFileName($this->url) . "\n";
        print "Нажмите Enter что бы продолжить...\n";
        readline();
        Menu::showMainMenu();
    }

    private function parseImages(string $url)
    {
        $sources = array();
        $html = file_get_contents($url);
        $images = explode('<img', $html);
        unset($images[0]);
        foreach($images as $image) {
            $arImg = explode('src="', $image);
            $arImg = explode('"', $arImg[1]);
            array_push($sources, $arImg[0]);
        }

        foreach ($sources as $source) {
            array_push($this->imagesArray, Checker::imgSrcBuilder($source, $url, $this->url));
        }
    }

}